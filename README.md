This is SITTV's website. It's hosted by Gitlab at [sittv.gitlab.io](https://sittv.gitlab.io/) and by Cloudflare at [sittv.org](https://sittv.org/). The site is written in React and was composed by Eric S. Londres in the summer of 2019.

The project was created with [Create-React-App](https://facebook.github.io/create-react-app/), so the standard CRA development and build script are available, such as `npm run`.
