import React from 'react';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Body from './Body';
import Process from './Process';
import Links from './Links';
import Header from './Header';
import Directory from './Directory';
import './Navigation.css';

const Navigation = () => (
    <Router>
      <header>
        <Header/>
        <nav style={{ display: "flex", justifyContent: "space-evenly", paddingTop: "5%" }}>
          <Link to="/">Home</Link>
          <Link to="/productions/">Production Process</Link>
          <a href="https://goo.gl/forms/OVbdcpWzBm" target="_blank" rel="noopener noreferrer">Pilot Submission</a>
          <Link to="/links">Links</Link>
          <Link to="/directory">Reviews</Link>
        </nav>
      </header>

      <main style={{ flex: "1 0 auto" }}>
        <Route exact path="/" component={Body}/>
        <Route path="/productions" component={Process}/>
        <Route path="/links" component={Links}/>
        <Route path="/directory" component={Directory}/>
      </main>
    </Router>
);

export default Navigation;
