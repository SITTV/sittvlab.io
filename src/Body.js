import React from 'react';
import YouTube from 'react-youtube';
import './Body.css';

const id = 'https://www.youtube.com/playlist?list=UUo1ZtfOBFmE2Zy91-Ptv9Wg&playnext=1&index=1';

const Body = () => (
    <div className="body">
      <YouTube
        videoId={id}
        containerClassName='iframe-host'
      />
      <div className="subscribe-button">
        <script src="https://apis.google.com/js/platform.js"></script>
        <div className="g-ytsubscribe" data-channel="UCo1ZtfOBFmE2Zy91-Ptv9Wg" data-layout="full" data-theme="dark" data-count="default"></div>
      </div>
      <p>
        SITTV is Stevens’s campuswide audio/visual media organization. We do all kinds of productions from shorts to television shows to feature-length films. We highly encourage new members to join! We meet on Mondays at 9pm in the SITTV Studio, Humphreys Hall red door across from the North Building.
      </p>
    </div>
);

export default Body;
