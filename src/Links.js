import React from 'react';

const Links = () => (
  <p style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
    <h1>Important Links</h1>
    <ul>
      <li><a href="https://sittv.slack.com/" target="_blank" rel="noopener noreferrer">SITTV Slack</a></li>
      <li><a href="https://youtube.com/sittv?sub_confirmation=1" target="_blank" rel="noopener noreferrer">SITTV on YouTube</a></li>
      <li><a href="https://goo.gl/forms/FcwfmbcNg2" target="_blank" rel="noopener noreferrer">Equipment Sign-Out Form</a></li>
      <li><a href="https://forms.office.com/Pages/ResponsePage.aspx?id=7GkajbUDRUOuIdrREvX7TyEuoQ7D5gJMqSSDs4XFRapUNE5WTElIRlg5UFhOSDAyTE9OTTZQUFZOUC4u" target="_blank" rel="noopener noreferrer">Swipe Access Request Form</a></li>
    </ul>
  </p>
);

export default Links;
