import React from 'react';
import './Footer.css';

const Footer = () => (
    <footer>
      <div className="footer-container">
        <div>To contact SITTV, send an email to <a href="mailto:sittv@stevens.edu">sittv@stevens.edu</a>.</div>
        <div>Copyright 2019 by SITTV. All rights reserved.</div>
      </div>
    </footer>
);

export default Footer;
