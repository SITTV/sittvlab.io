import React from 'react';
import Navigation from './Navigation';
import Footer from './Footer';

function App() {
  return (
      <div style={{
          display: "flex",
          flexDirection: "column",
          minHeight: "100vh",
      }}>
        <Navigation/>
        <Footer/>
      </div>
  );
}

export default App;
