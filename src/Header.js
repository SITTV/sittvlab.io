import React from 'react';
import logo from './logo.png';
import './Header.css';

const Header = () => (
    <div className="header">
      <a href="https://youtube.com/sittv?sub_confirmation=1">
        <img src={logo} className="logo" alt="sittv logo"/>
      </a>
    </div>
);

export default Header;
