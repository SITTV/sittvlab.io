import React from 'react';

const Directory = () => (
    <p style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
        <h3>MA123 - Series, Vectors, Functions, and Surfaces</h3>
        <a href="https://youtu.be/CgljybwztWU" target="_blank" rel="noopener noreferrer">Exam 1 Review - Spring 2016</a>
        <h3>MA124 - Calculus of Two Variables</h3>
        <a href="https://youtu.be/AscMEyV0TPI" target="_blank" rel="noopener noreferrer">Midterm Review - Spring 2017</a>
        <h3>MA221 - Differential Equations</h3>
        <a href="https://youtu.be/s61mRhDlynQ" target="_blank" rel="noopener noreferrer">Exam 1 Review - Fall 2023</a>
        <a href="https://youtu.be/7qOSwY-VdbY" target="_blank" rel="noopener noreferrer">Final Review - Fall 2019</a>
        <a href="https://youtu.be/VFJmaShIdnA" target="_blank" rel="noopener noreferrer">Exam 3 Review - Fall 2019</a>
        <a href="https://youtu.be/fUNj0rLtfPg" target="_blank" rel="noopener noreferrer">Exam 2 Review - Fall 2019</a>
        <a href="https://youtu.be/RZeWz3Hq2CE" target="_blank" rel="noopener noreferrer">Exam 1 Review - Fall 2019</a>
        <a href="https://youtu.be/OeenJJhyuDM" target="_blank" rel="noopener noreferrer">Final Review - Fall 2018 - Part Two</a>
        <a href="https://youtu.be/KtsbCS5WmaE" target="_blank" rel="noopener noreferrer">Final Review - Fall 2018 - Part One</a>
        <a href="https://youtu.be/k1qIBcFNbsQ" target="_blank" rel="noopener noreferrer">Final Review - Fall 2017</a>
        <a href="https://youtu.be/er4EU2v-CAA" target="_blank" rel="noopener noreferrer">Exam 3 Review - Fall 2016</a>
        <a href="https://youtu.be/aHc-vCgKxyI" target="_blank" rel="noopener noreferrer">Exam 2 Review - Fall 2016</a>
        <a href="https://youtu.be/f1NmeMPcYDY" target="_blank" rel="noopener noreferrer">Exam 1 Review - Fall 2016</a>
        <a href="https://youtu.be/_2O41Y_uDxo" target="_blank" rel="noopener noreferrer">Final Review - Spring 2016 - Part Two</a>
        <a href="https://youtu.be/aQq1WVsgEGQ" target="_blank" rel="noopener noreferrer">Final Review - Spring 2016 - Part One</a>
        <a href="https://youtu.be/ntqLBApKvAE" target="_blank" rel="noopener noreferrer">Exam 3 Review - Spring 2016</a>
        <a href="https://youtu.be/MYeYB9Ls-Jg" target="_blank" rel="noopener noreferrer">Exam 2 Review - Spring 2016</a>
        <a href="https://youtu.be/fFoUHLyKa2k" target="_blank" rel="noopener noreferrer">Exam 1 Review - Spring 2016</a>
        <h3>MA227 - Multivariable Calculus</h3>
        <a href="https://youtu.be/k1qIBcFNbsQ" target="_blank" rel="noopener noreferrer">Final Review - Fall 2017</a>
        <h3>PEP111 - Mechanics</h3>
        <a href="https://youtu.be/1LlQ8h8sCxQ" target="_blank" rel="noopener noreferrer">Exam 2 Review - Fall 2016</a>
        <a href="https://youtu.be/hYTbqH5kIQI" target="_blank" rel="noopener noreferrer">Exam 1 Review - Fall 2016</a>
        <h3>PEP112 - Electricity and Magnetism</h3>
        <a href="https://youtu.be/oSW7VNLiQOw" target="_blank" rel="noopener noreferrer">Exam 1 Review - Fall 2016</a>
        <h3>E126 - Mechanics of Solids</h3>
        <a href="https://youtu.be/_yn93_6nXRE" target="_blank" rel="noopener noreferrer">Final Review - Fall 2018</a>
    </p>
);

export default Directory;
